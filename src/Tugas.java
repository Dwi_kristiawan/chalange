import java.io.IOException;
import java.util.Scanner;

public class Tugas {
    public static void main(String[] args) {
        home();
    }

    static void switchMenu(int tmp, int pos) {
        switch(tmp) {
            case 2:
                switch(pos) {
                    case 0:
                    default:
                        return;
                    case 1:
                        luasMenu();
                        return;
                    case 2:
                        volumMenu();
                        return;
                }
            case 3:
                switch(pos) {
                    case 0:
                    default:
                        return;
                    case 1:
                        balok();
                        return;
                    case 2:
                        kubus();
                        return;
                    case 3:
                        tabung();
                        return;
                    case 99:
                        home();
                        return;
                }
            case 4:
                switch(pos) {
                    case 0:
                    default:
                        break;
                    case 1:
                        persegi();
                        break;
                    case 2:
                        lingkaran();
                        break;
                    case 3:
                        segitiga();
                        break;
                    case 4:
                        persegiPanjang();
                        break;
                    case 99:
                        home();
                }
        }

    }

    static boolean validasi(int tmp, int inputan) {
        for(int i = 0; i <= tmp; ++i) {
            if (i == inputan || inputan == 99) {
                switchMenu(tmp, inputan);
                return true;
            }
        }

        System.out.println(" inputan tidak sesuai ");
        return false;
    }

    static void home() {
        System.out.println("___________________________________");
        System.out.println("     Kalkulator Luas dan Volum");
        System.out.println("___________________________________");
        System.out.println(" MENU");
        System.out.println(" 1. Luas");
        System.out.println(" 2. Volum");
        System.out.println(" 0. Tutup Aplikasi");
        System.out.println("___________________________________");
        int tmp = 2;
        boolean status = validasi(tmp, inputInt());
        if (!status) {
            home();
        }

    }

    static void luasMenu() {
        System.out.println("___________________________________");
        System.out.println("  Pilih Bidang Yang Akan Dihitung");
        System.out.println("___________________________________");
        System.out.println(" 1. Persegi");
        System.out.println(" 2. Lingkaran");
        System.out.println(" 3. Segita");
        System.out.println(" 4. Persegi Panjang");
        System.out.println(" 99. kembali ke menu sebelumnya");
        System.out.println("___________________________________");
        int tmp = 4;
        boolean status = validasi(tmp, inputInt());
        if (!status) {
            luasMenu();
        }

    }

    static void volumMenu() {
        System.out.println("___________________________________");
        System.out.println("  Pilih Bidang Yang Akan Dihitung");
        System.out.println("___________________________________");
        System.out.println(" 1. Balok");
        System.out.println(" 2. Kubus");
        System.out.println(" 3. Tabung");
        System.out.println(" 99. kembali ke menu sebelumnya");
        System.out.println("___________________________________");
        int tmp = 3;
        boolean status = validasi(tmp, inputInt());
        if (!status) {
            home();
        }

    }

    static int inputInt() {
        Scanner scan = new Scanner(System.in);
        return scan.nextInt();
    }

    static void pressAny() {
        System.out.println("  tekan apasaja untuk ke menu utama ");

        try {
            System.in.read();
        } catch (IOException var1) {
        }

        home();
    }

    static void segitiga() {
        System.out.println("___________________________________");
        System.out.println("        Anda Memilih Segitiga");
        System.out.println("___________________________________");
        System.out.println("inputkan alas ");
        int alas = inputInt();
        System.out.println("inputkan tinggi");
        int tinggi = inputInt();
        float luas = (float)alas * (float)tinggi / 2.0F;
        System.out.println("Luas dari Segitiga adalah " + luas);
        System.out.println("___________________________________");
        pressAny();
    }

    static void lingkaran() {
        System.out.println("___________________________________");
        System.out.println("      Anda Memilih Lingkaran");
        System.out.println("___________________________________");
        System.out.println("inputkan jari-jari ");
        int jari_jari = inputInt();
        float luas = 3.14F * (float)jari_jari * (float)jari_jari;
        System.out.println("Luas dari Segitiga adalah " + luas);
        System.out.println("___________________________________");
        pressAny();
    }

    static void persegi() {
        System.out.println("___________________________________");
        System.out.println("         Anda Memilih Persegi");
        System.out.println("___________________________________");
        System.out.println("inputkan sisi ");
        int sisi = inputInt();
        float luas = (float)(sisi * sisi);
        System.out.println("Luas dari persegi adalah " + luas);
        System.out.println("___________________________________");
        pressAny();
    }

    static void persegiPanjang() {
        System.out.println("___________________________________");
        System.out.println("    Anda Memilih persegi panjang");
        System.out.println("___________________________________");
        System.out.println("inputkan panjang ");
        int panjang = inputInt();
        System.out.println("inputkan lebar");
        int lebar = inputInt();
        float luas = (float)(panjang * lebar);
        System.out.println("Luas dari Segitiga adalah " + luas);
        System.out.println("___________________________________");
        pressAny();
    }

    static void kubus() {
        System.out.println("___________________________________");
        System.out.println("         Anda Memilih Kubus");
        System.out.println("___________________________________");
        System.out.println("inputkan alas ");
        int sisi = inputInt();
        float volume = (float)(sisi * sisi * sisi);
        System.out.println("Volume Kubus Adalah " + volume);
        System.out.println("___________________________________");
        pressAny();
    }

    static void balok() {
        System.out.println("___________________________________");
        System.out.println("         Anda Memilih Balok");
        System.out.println("___________________________________");
        System.out.println("inputkan panjang ");
        int panjang = inputInt();
        System.out.println("inputkan lebar ");
        int lebar = inputInt();
        System.out.println("inputkan tinggi");
        int tinggi = inputInt();
        float volume = (float)(panjang * lebar * tinggi);
        System.out.println("Volume Balok = " + panjang + "*" + lebar + "*" + tinggi + " = " + volume);
        System.out.println("___________________________________");
        pressAny();
    }

    static void tabung() {
        System.out.println("___________________________________");
        System.out.println("         Anda Memilih Tabung");
        System.out.println("___________________________________");
        System.out.println("inputkan jari jari ");
        int jari_jari = inputInt();
        System.out.println("inputkan tinggi");
        int tinggi = inputInt();
        float volume = (float)(jari_jari * tinggi);
        System.out.println("Volume Tabung = " + jari_jari + "*" + tinggi + " = " + volume);
        System.out.println("___________________________________");
        pressAny();
    }
}
